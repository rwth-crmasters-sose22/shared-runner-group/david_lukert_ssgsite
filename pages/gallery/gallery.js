//Content handling the /gallery page 

import Head from 'next/head';
import Link from 'next/link';
import Script from 'next/script';
import Image from 'next/image';
import Layout from '../../components/layout';
import utilStyles from '../../styles/utils.module.css';
import { getSortedPostsData } from '../../lib/pictures';
import { remark } from 'remark';
import html from 'remark-html';
import matter from 'gray-matter';

export async function getStaticProps() {
    const allPostsData = getSortedPostsData();
    return {
        props: {
            allPostsData,
        },
    };
}

export default function Gallery({ allPostsData }) {
    return (
        <Layout>    
            <Head>
                <title>David's Gallery</title>
            </Head>

            <h1 className={utilStyles.headingXl}>gallery</h1>

            <div>
                <ul className={utilStyles.list}>
                    {allPostsData.map(({ id, description, date, title, image, image2, image3 }) => (
                    <li className={utilStyles.cardpic} key={id}>
                        <h3>{title}</h3>
                        <br />
                        {description}
                        <br />
                        {date}
                        <br />
                        <br />
                        <div className={utilStyles.imgg}>
                            <Image 
                                src={image}
                                height={337}
                                width={480}
                            />
                        </div>
                        <div className={utilStyles.imgg}>
                            <Image 
                                src={image2}
                                height={337}
                                width={480}
                            />
                        </div>
                        <div className={utilStyles.imgg}>
                        <Image 
                            src={image3}
                            height={337}
                            width={480}
                        />
                        </div>
                    </li>
                  ))}
                </ul>
            </div>
        </Layout>
    );
}