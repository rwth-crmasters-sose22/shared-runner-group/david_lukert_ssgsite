//content handling the /about page 

import Head from 'next/head';
import Link from 'next/link';
import Script from 'next/script';
import Layout from '../../components/layout';
import utilStyles from '../../styles/utils.module.css';

export default function About() {
    return (
        <Layout>    
            <Head>
                <title>David's about page</title>
            </Head>

            <h1 className={utilStyles.headingXl}>about</h1>
            <div className={utilStyles.cardpic}>
                <h3>right now</h3>
                <p>
                    May I introduce myself? My name is David Lukert and I'm currently 26 years old. This april I started my masters programm at the RWTH-Aachen. The programm is called <strong>Construction and Robotics</strong> and 
                    is an international masters program focusing on the future of the construction site and industry. I really like the masters programm, because you learn a lot about different backgrounds, cultural and work area wise. 
                    I like the interdiciplinary approach and the broad variety of things we can learn. Visit their  <Link href="https://cr.rwth-aachen.de/"><a>offical site</a></Link> for more informations. I have the drive to learn new things regarding technologies of any kind and I want to try to blend them with architecture. My vision is
                    to work towards a new technicological yet antrophological approach to see buildings and space in general.   
                </p>
            </div>
            <div className={utilStyles.cardpic}>
                <h3>skills</h3>
                <p>
                    Following a list with skills I obtained and/ or working to improve:
                </p>
                <ul> <strong>software</strong>
                    <li>Rhino 3D and Grasshopper (advanced)</li>
                    <li>Autocad</li>
                    <li>Tenado 3D metall</li>
                    <li>Adobe Illustrator </li>
                    <li>Adobe Photoshop</li>
                </ul>
                <ul> <strong>programming</strong>
                    <li>Python (basic)</li>
                    <li>HTML & CSS (basic)</li>
                    <li>Javascript (learner)</li>
                    <li>C# (learner)</li>
                    <li>visual programming (Grasshopper)</li>
                </ul>
                <ul> <strong>soft skills</strong>
                    <li>Language English</li>
                    <li>Language French (basic)</li>
                    <li>Audio Recording Software (Ableton)</li>
                </ul>
            </div>
        </Layout>
    );
}