//Layout page - importet to site via: import Layout, { siteTitle } from '../components/layout';

import Head from 'next/head';
import Image from 'next/image';
import styles from './layout.module.css';
import utilStyles from '../styles/utils.module.css';
import Link from 'next/link';

const name = 'David Lukert';
export const siteTitle = "David's SSG-Site";

export default function Layout({ children, home }) {
  return (
    <div className={styles.container}>
      <Head>
        <meta
          name="description"
          content="This is the Static Website of David Lukert"
        />
        <meta name="og:title" content={siteTitle} />
        <meta name="twitter:card" content="summary_large_image" />
      </Head>
      
      <div className={styles.sidebar} id={styles.column}>
          <Link href="/">
            <a>
              <Image
                priority
                src="/../public/profile picture_lukert.jpg"
                className={utilStyles.borderCircle}
                height={120}
                width={120}
                alt={name}
              />
            </a>
          </Link>
            <h2 className={utilStyles.headingXl}>
              <Link href="/">
                <a className={utilStyles.heading2XL}>{name}</a>
              </Link>
            </h2>
            <div className={utilStyles.grid}>
                <div className={utilStyles.card}>
                    <Link href="/gallery/gallery">
                        <a className={utilStyles.headingLg}>gallery</a>
                    </Link>
                    <p>Some projects I worked on in the past</p>
                </div>
                <div className={utilStyles.card}>
                    <Link href="/about/about">
                      <a className={utilStyles.headingLg}>about</a>
                    </Link>
                    <p>My background, motivation and philosophy</p>
                </div>
            </div>
          	<div className={utilStyles.grid}>
              <div className={utilStyles.headingLg}>
                <p className={utilStyles.lightText}>Contact Me</p>
                <a className={utilStyles.iconLink} href='https://www.linkedin.com/in/david-lukert-1954b9237/'>
                  <Image 
                    src="/../public/LI-In-Bug.png" 
                    width={38.1}
                    height={32.4}
                    alt="Linkedin"
                  />
                </a>
                <a className={utilStyles.iconLink} href='mailto:david.lukert@rwth-aachen.de'>
                  <Image 
                    src="/../public/mail_icon.png" 
                    width={40}
                    height={40}
                    alt="Linkedin"
                  />
                </a>
                <a className={utilStyles.iconLink} href='https://github.com/lastthought15'>
                  <Image 
                    src="/../public/GitHub-Mark-32px.png" 
                    width={40}
                    height={40}
                    alt="Linkedin"
                  />
                </a>
              </div>  
            </div>
         
      </div>

      <div className={styles.rightside} id={styles.column}>
            <div className={styles.topright}>
            </div>
            <div className={styles.maincont}>
                <main className={styles.maincont}>{children}</main>
                
            </div>
            <div className={styles.footer}> 
              {!home && (
                  <div className={styles.backToHome}>
                    <Link href="/">
                      <a>← Back to home</a>
                    </Link>
                  </div>
                )}
              <p className={styles.backToHome}>© 2022 by David Lukert</p>
            </div>
      </div>
    </div>

    

  );
}