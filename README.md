## Repo for a SSG-Site 

This is a SSG Site build with [__next.js__](https://nextjs.org/) and deployed via [__Vercel__](https://ssg-site-01.vercel.app/). 

The deployed site can be found [here](https://ssg-site-01.vercel.app/).
